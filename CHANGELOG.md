<!-- Keep a Changelog guide -> https://keepachangelog.com -->

# oat_deveco_plugin Changelog

## [Unreleased]
### Added
- Initial scaffold created from [IntelliJ Platform Plugin Template](https://github.com/JetBrains/intellij-platform-plugin-template)
